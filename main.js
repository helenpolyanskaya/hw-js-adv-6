// Асінхороность, це коли основний код жс в одной реальності, а треба зробити щось, що не тормозить(не стаивть на паузу) виконання коду, а робить код в параллельної реальності 


class Location {

    constructor (continent, country, region, city, district) {
        this.continent = continent;
        this.country = country;
        this.region = region;
        this.city = city;
        this.district = district;
    }

    renderLocation(){
        let app = document.getElementById('app');
        app.insertAdjacentHTML('beforeend',`<div style="color: purple; margin-top: 10px">Continent: ${this.continent} / Country: ${this.country} / Region: ${this.region} / City: ${this.city} / District: ${this.district} </div>`)
    }  
}

async function getIP(){
    let ipAddress = await fetch('https://api.ipify.org/?format=json');
    let ip = await ipAddress.json();
    let addressResponse = await fetch('http://ip-api.com/json/'+ip.ip+"?fields=continent,country,region,city,district");
    let address = await addressResponse.json();
    console.log(address)
    let loc = new Location(address.continent, address.country, address.region, address.city, address.district)
    loc.renderLocation();
}

let button = document.getElementById("buttonIp");
button.addEventListener("click", getIP);

